from setuptools import setup
import os

# EDIT this variable to point to your workflow subdir
workflow_name = "snake_element"

package_files = [
    os.path.relpath(os.path.join(d, f), workflow_name)
    for d, _, files in os.walk(workflow_name)
    for f in files
]

setup(
    name=workflow_name,
    version="0.2.4",
    packages=[workflow_name],
    python_requires=">=3.6",
    install_requires=[
        "snakemake",
        "scikit-learn",
        "statsmodels",
        "pandas",
        "joblib",
        "plotly",
        "logomaker",
        "bcbio-gff",
        "biopython",
        "plotly"
    ],
    entry_points={
        "console_scripts": [
            f"{workflow_name} = {workflow_name}.__main__:main",
            f"promoter_embedding = {workflow_name}.promoter_embedding:main",
            f"promoter_analysis = {workflow_name}.__main__:clean_cli",
            f"extract_promoters = {workflow_name}.extract_promoters:cli_main"
        ]
    },
    package_data={
        f"{workflow_name}": package_files
    },
)
