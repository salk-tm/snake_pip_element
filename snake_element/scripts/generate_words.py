from itertools import product
from element_cluster import reverse_comp


def gen_canonical_kmers(minlen, maxlen):
    for l in range(minlen, maxlen):
        for kmer in product("ACTG", repeat=l):
            kmer = ''.join(kmer)
            rev_comp = reverse_comp(kmer)
            if(rev_comp >= kmer):
                yield(kmer)


def main(minlen, maxlen, outfile):
    with open(outfile, "w") as fout:
        for kmer in gen_canonical_kmers(minlen, maxlen):
            fout.write(kmer + "\n")


def snakemain():
    outfile = snakemake.output[0]
    minlen = snakemake.params["minlen"]
    maxlen = snakemake.params["maxlen"]
    main(minlen, maxlen, outfile)


if(__name__ == "__main__"):
    snakemain()

