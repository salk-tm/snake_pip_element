import pandas
from scipy.stats import fisher_exact, ttest_ind
from statsmodels.stats.multitest import multipletests
from collections import Counter, defaultdict
from misc import load_fasta, load_gene_list


def get_fimo_counts(fpath):
    dfs = pandas.read_csv(fpath, sep="\t", chunksize=50000)
    # counts[motif_id][gene_id] = count
    counts = defaultdict(lambda : Counter())
    nrows = 0
    for df in dfs:
        for gid, mid in zip(df.sequence_name, df.motif_id):
            nrows += 1
            counts[mid][gid] += 1
    return counts


def fisher_analysis(counts, all_genes, gene_set):
    m_gs = {m: sum(1 for g in gs if g in gene_set) for m, gs in counts.items()}
    m_ngs = {m: len(gs) - m_gs[m] for m, gs in counts.items()}
    nm_gs = {m: len(gene_set) - c for m, c in m_gs.items()}
    nm_ngs = {m: len(all_genes) - len(gene_set) - c for m, c in m_ngs.items()}
    fisher_results = {
        m: fisher_exact(
            [
                [m_gs[m], m_ngs[m]],
                [nm_gs[m], nm_ngs[m]]
            ],
            alternative="greater"
        )
        for m in counts
    }
    ret = pandas.DataFrame({
        "motif_and_gs": m_gs,
        "motif_and_not_gs": m_ngs,
        "not_motif_and_gs": nm_gs,
        "not_motif_and_not_gs": nm_ngs,
        'fisher_P': {m: r[1] for m, r in fisher_results.items()},
        'fisher_ratio': {m: r[0] for m, r in fisher_results.items()},
    })
    return ret


def ttest_analysis(counts, all_genes, gene_set):
    """ performs a ttest
    """
    not_gene_set = all_genes - gene_set
    pvals = {
        m: ttest_ind(
            [d[g] for g in gene_set],
            [d[g] for g in not_gene_set],
            alternative="greater",
            equal_var=False
        )[1]
        for m, d in counts.items()
    }
    outmean = {m: sum(d[g] for g in not_gene_set)/len(not_gene_set) for m, d in counts.items()}
    inmean = {m: sum(d[g] for g in gene_set)/len(gene_set) for m, d in counts.items()}
    ret = pandas.DataFrame({
        "ttest_P": pvals,
        "in_mean": inmean,
        "out_mean": outmean,
    })
    ret["ttest_ratio"] = ret.in_mean / ret.out_mean
    return ret


def main(gl_files, fimo_files, fastas, job_names, out_all, out_sig):
    import time
    results = {}
    for fimo_file, gl, fasta, job_name in zip(fimo_files, gl_files, fastas, job_names):
        print(job_name)
        start = time.time()
        fimo_counts = get_fimo_counts(fimo_file)
        fasta_df = load_fasta(fasta)
        genes = set(fasta_df.ID)
        gene_set = set(load_gene_list(gl))    
        temp1 = fisher_analysis(fimo_counts, genes, gene_set)
        temp2 = ttest_analysis(fimo_counts, genes, gene_set)
        results[job_name] = pandas.concat([temp1, temp2], axis=1)
        print(time.time() - start)
    all_results = pandas.concat([results[r] for r in job_names], keys=job_names, axis=0).reset_index()
    all_results = all_results.rename({"level_0": "job_id", "level_1": "motif_id"}, axis=1)
    all_results["ttest_FDR"] = multipletests(all_results.ttest_P, method="fdr_bh")[1]
    all_results["fisher_FDR"] = multipletests(all_results.fisher_P, method="fdr_bh")[1]
    all_results[all_results.fisher_FDR < 0.05].to_csv(out_sig, sep="\t", index=None)
    all_results.to_csv(out_all, sep="\t", index=None)


def snakemain():
    fimos = snakemake.input["fimos"]
    gl = snakemake.input["gene_lists"]
    fastas = snakemake.input["fastas"]
    job_names = snakemake.params["job_names"]
    out_all = snakemake.output[1]
    out_sig = snakemake.output[0]
    main(gl, fimos, fastas, job_names, out_all, out_sig)


if(__name__ == "__main__"):
    snakemain()
