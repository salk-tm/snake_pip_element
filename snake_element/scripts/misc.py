import json
import os
import pandas
from typing import List, Optional
from itertools import groupby


def load_json(fpath: str):
    with open(fpath) as fin:
        return json.load(fin)


def split_gene_list(fpath: str, outdir: str):
    genes = load_json(fpath)
    os.makedirs(outdir, exist_ok=True)
    for k, l in genes.items():
        with open(f"{outdir}/{k}.txt", "w") as fout:
            for g in l:
                fout.write(g)
                fout.write("\n")


def trim_element_background(
    ele_bground: str,
    ele_out: str,
    words_out: str
):
    data = pandas.read_csv(ele_bground, header=None, sep="\t")
    data = data[data.iloc[:, 1:].sum(axis=1) != 0]
    data.to_csv(ele_out, header=None, sep="\t", index=False)
    with open(words_out, 'w') as fout:
        for word in data[0]:
            fout.write(word)
            fout.write("\n")


def load_gene_list(fpath: str) -> List[str]:
    with open(fpath) as fin:
        return fin.read().split()


def merge_lists(
    gene_lists: List[str],
    outpath: str,
    keys: Optional[List[str]] = None
):
    if(keys is None):
        keys = gene_lists
    d = {k: load_gene_list(l) for k, l in zip(keys, gene_lists)}
    with open(outpath, "w") as fout:
        json.dump(d, fout, indent=4)


def load_fasta(f: str) -> pandas.DataFrame:
    """ reads fasta data located at f and returns a dataframe of shape 3xN
        where N is the number of entries in the fasta file. The columns are:
        ID, INFO, SEQ. 
    """
    with open(f) as fin:
        grouper = groupby(fin, lambda l: l[0]==">")
        grouper = ("".join(v) for k, v in grouper)
        data = {h[1:-1]: v.replace("\n", "") for h, v in zip(grouper, grouper)}
    ret = pandas.Series(data).to_frame().reset_index()
    ret.columns = ["HEADER", "SEQ"]
    ret["ID"] = ret.HEADER.str.split().str[0]
    ret["INFO"] = ret.HEADER.str.split().str[1:].str.join(" ")
    return ret[["ID", "INFO", "SEQ"]]
