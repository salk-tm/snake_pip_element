from typing import Set, List
import pandas
import numpy
import itertools
from functools import partial
from sklearn.cluster import AgglomerativeClustering
import os
import time


# This is used for type declarations
DF = pandas.DataFrame


def load_element(f: str):
    cols = [
        "TotalHits", "MeanHits", "HitSTD", "HitP", "HitSeqs",
        "PressenceP", "MeanP"
    ]
    try:
        df = pandas.read_csv(f, sep="\t", header=None, index_col=0)
        df.columns = cols[:df.shape[1]]
    except pandas.errors.EmptyDataError:
        df = pandas.DataFrame([], columns=cols)
    return df


def get_signal_deltas(
    sig_kmers: Set[str],
    ele_dfs: List[DF],
    bg_dfs: List[DF]
) -> DF:
    """ Compute distances between kmers based on how those kmers behave in our
        experiments
    """
    all_sigs = list(sig_kmers)
    abs_signal = pandas.DataFrame({
        i: el.HitP - bg.HitP
        for i, (el, bg) in enumerate(zip(ele_dfs, bg_dfs))
    })
    signal_deltas = pandas.DataFrame({
        k1: ((abs_signal.loc[k1] - abs_signal.loc[all_sigs])**2).mean(axis=1)
        for k1 in all_sigs
    })
    adj_deltas = signal_deltas * 100
    return adj_deltas


def overlap_score(
    kmer1: str,
    kmer2: str,
    match: int = 3,
    gap: int = -2,
    mis: int = -3
) -> int:
    """ do smith waterman based overlap alignment of a and b and return score

        >>> overlap_score("AAAT", "CAAA")
        9
        >>> overlap_score("ACAT", "CAAA")
        4
    """
    H = numpy.zeros((len(kmer1) + 1, len(kmer2) + 1), int)
    for i in range(len(kmer1)):
        for j in range(len(kmer2)):
            diag = H[i, j] + match if(kmer1[i] == kmer2[j]) else mis
            delete = H[i, j + 1] + gap
            insert = H[i + 1, j] + gap
            H[i + 1, j + 1] = max(diag, delete, insert)
    return max(max(H[:, -1]), max(H[-1, :]))



rev_comp_kmers = {c1: c2 for c1, c2 in zip("ATCG", "TAGC")}


def reverse_comp(kmer):
    return "".join(rev_comp_kmers[c] for c in reversed(kmer))


def kmer_distance(a: str, b: str) -> float:
    """ compute distance between two kmers. This distance is defined as the
        max theoretic overlap_score for the kmers minus the actual overlap
        score for the kmers divided by that same max theoretic score after
        allowing for reverse compliment if it improves alignment
    """
    overlap = partial(overlap_score, match=3, gap=-2, mis=-3)
    max_score = min(len(a), len(b)) * 3
    best_score = max(overlap(a, b), overlap(a, reverse_comp(b)))
    return (max_score - best_score) / max_score


def load_data(elements, filtereds, bgrounds):
    """ automates loading all element data
    """
    sigs = [load_element(f) for f in filtereds]
    dfs = {fpath: load_element(fpath) for fpath in set(elements + bgrounds)}
    ele_dfs = [dfs[f] for f in elements]
    bg_dfs = [dfs[f] for f in bgrounds]
    return sigs, ele_dfs, bg_dfs


def main(elements, filtereds, bgrounds, eps, bias, outdir):
    # load data
    all_sigs, ele_dfs, bg_dfs = load_data(elements, filtereds, bgrounds)
    all_sigs = {m for df in all_sigs for m in df.index}
    # all_sigs = set(list(all_sigs)[:10])

    # compute distances
    sig_deltas = get_signal_deltas(all_sigs, ele_dfs, bg_dfs)
    seq_deltas = pandas.DataFrame({
        k1: {k2: kmer_distance(k1, k2) for k2 in all_sigs}
        for k1 in all_sigs
    })
    combined_deltas = sig_deltas * bias + seq_deltas * (1 - bias)

    ac = AgglomerativeClustering(
        n_clusters=None,
        metric="precomputed",
        distance_threshold=eps,
        linkage="average"
    )
    clusters = ac.fit_predict(combined_deltas)
    cluster_map = {}
    for kmer, c in zip(combined_deltas.index, clusters):
        s = cluster_map.get(c, set())
        s.add(kmer)
        cluster_map[c] = s

    os.makedirs(outdir, exist_ok=True)
    for c, kmers in cluster_map.items():
        if(len(kmers) > 1):
            with open(f"{outdir}/cluster_{c}.fasta", "w") as fout:
                for i, kmer in enumerate(kmers):
                    fout.write(f">kmer_{i}\n{kmer}\n")


def snakemain():
    print("starting clustering")
    filtereds = snakemake.input["filtered"]
    raw = snakemake.input["raw"]
    bgs = snakemake.input["bgrounds"]
    eps = snakemake.params["epsilon"]
    bias = snakemake.params["bias"]
    outdir = snakemake.output[0]
    main(raw, filtereds, bgs, eps, bias, outdir)


if(__name__ == "__main__"):
    snakemain()