import matplotlib
matplotlib.use('Agg')
import plotly
from plotly import express
from element_cluster import load_element, get_signal_deltas, reverse_comp, load_data
import pandas
import os
import logomaker
from matplotlib import pyplot
from io import StringIO
import json


def alignment_to_canon_kmer(aln):
    kmer = aln.replace("-", "")
    rev_comp = reverse_comp(kmer)
    if(kmer <= rev_comp):
        return kmer
    else:
        return rev_comp


def load_clusters(aligndir):
    ret = {}
    for f in os.listdir(aligndir):
        if(f.endswith(".fasta")):
            with open(os.path.join(aligndir, f)) as fin:
                ret[f] = fin.read().split()[1::2]
    ret = {f: [s.upper() for s in sorted(aln)] for f, aln in ret.items()}
    return ret


def cluster_to_ratios(cluster):
    df_cluster = pandas.DataFrame([list(k) for k in cluster]).transpose()
    df_ratios = pandas.DataFrame({
        c: (df_cluster == c).astype("int").mean(axis=1)
        for c in "ACGT"
    })
    return df_ratios


def encode_pyplot_fig(clustername):
    with StringIO() as tempout:
        pyplot.savefig(tempout, format="svg")
        ret = tempout.getvalue()
    ret = ret.replace("<svg", '<svg style="pointer-events:none;"')
    # ret = ret.replace
    ret = (
        f'<button style="background-color:transparent;border:0;padding:0;" class="motif" id="{clustername}">'
        '<svg width=80% viewBox="0 0 920 240" style="pointer-events:none"><g>'
        f'{ret}'
        "</g></svg></button>"
    )
    return ret


def main(filtered, raw, background, alignments_dir, jobnames, template, outpath):
    # load all data
    sigs, ele_dfs, bg_dfs = load_data(raw, filtered, background)
    all_sigs = {m for df in sigs for m in df.index}
    split_sigs = {n: {m for m in df.index} for n, df in zip(jobnames, sigs)}

    abs_signal = pandas.DataFrame({
        n: el.loc[list(all_sigs)].HitP - bg.loc[list(all_sigs)].HitP
        for n, el, bg in zip(jobnames, ele_dfs, bg_dfs)
    })
    clusters = load_clusters(alignments_dir)

    # build output
    data = {}
    logos = []
    for fname, cluster in clusters.items():
        # build sequence logo
        df_ratios = cluster_to_ratios(cluster)
        logo = logomaker.Logo(df_ratios, shade_below=.5, fade_below=.5)
        svg_text = encode_pyplot_fig(fname)
        pyplot.close("all")
        logos.append(svg_text)

        # extract signal
        canon = [alignment_to_canon_kmer(k) for k in cluster]
        heat_data = abs_signal.loc[canon]
        heat_data.index = cluster
        sig_data = pandas.DataFrame({
            samp : {kmer : kmer in split_sigs[samp] for kmer in canon}
            for samp in heat_data.columns
        })
        data[fname] = {
            "z":heat_data.to_numpy().tolist(),
            "y":heat_data.index.tolist(),
            "x": heat_data.columns.tolist(),
            "t": sig_data.to_numpy().tolist(),
        }
    
    # write output html
    with open(template) as fin:
        template_text = fin.read()
    with open(outpath, "w") as fout:
        template_text = template_text.replace("{all_data}", json.dumps(data))
        template_text = template_text.replace("{plots}", "\n".join(logos))
        fout.write(template_text)


def snakemain():
    raw = snakemake.input["raw"]
    filtered = snakemake.input["filtered"]
    bgrounds = snakemake.input["bgrounds"]
    align_dir = snakemake.input["alignments"]
    template = snakemake.input["html_template"]
    out = snakemake.output[0]
    jobnames = snakemake.params["jobnames"]
    main(filtered, raw, bgrounds, align_dir, jobnames, template, out)


if(__name__ == "__main__"):
    snakemain()