import pandas
from summarize_element import load_clusters


def cluster_to_counts(cluster):
    df_cluster = pandas.DataFrame([list(k) for k in cluster]).transpose()
    df_counts = pandas.DataFrame({
        c: (df_cluster == c).astype("int").sum(axis=1)
        for c in "ACGT"
    })
    return df_counts


def main(cluster_dir, outfile):
    clusters = load_clusters(cluster_dir)
    counts = {f: cluster_to_counts(c) for f, c in clusters.items()}
    with open(outfile, "w") as fout:
        for f, c in counts.items():
            fout.write(f">{f}\n")
            c.to_csv(fout, sep="\t", index=False, header=False)


def snakemain():
    indir = snakemake.input[0]
    output = snakemake.output[0]
    main(indir, output)


if(__name__ == "__main__"):
    snakemain()
