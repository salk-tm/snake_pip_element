#!/usr/bin/env python3

import sys
import argparse
import random
import time
import warnings
from itertools import groupby
from collections import Counter
from pkg_resources import resource_filename
from string import Template
import json

import pandas
import numpy
import joblib
from scipy.stats import zscore, chi2_contingency
from statsmodels.stats.multitest import multipletests

from sklearn.manifold import Isomap
from sklearn.mixture import GaussianMixture
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.preprocessing import StandardScaler, Normalizer
from sklearn.pipeline import Pipeline

workflow = __name__.split('.')[0]
hexmers_path = resource_filename(workflow, 'kmers.6.txt')
plot_template = resource_filename(workflow, "embed_plot_template.html")


def custom_formatwarning(msg, *args, **kwargs):
    # ignore everything except the message
    return str(msg) + '\n'

warnings.formatwarning = custom_formatwarning


def load_list(f):
    with open(f) as fin:
        return fin.read().split()


def load_fasta(f):
    """ reads fasta data located at f and returns a dataframe of shape 3xN
        where N is the number of entries in the fasta file. The columns are:
        ID, INFO, SEQ. 
    """
    with open(f) as fin:
        grouper = groupby(fin, lambda l: l[0]==">")
        grouper = ("".join(v) for k, v in grouper)
        data = {h[1:-1]: v.replace("\n", "") for h, v in zip(grouper, grouper)}
    ret = pandas.Series(data).to_frame().reset_index()
    ret.columns = ["HEADER", "SEQ"]
    ret["ID"] = ret.HEADER.str.split().str[0]
    ret["INFO"] = ret.HEADER.str.split().str[1:].str.join(" ")
    return ret[["ID", "INFO", "SEQ"]]


def reverse_comp(kmer, compliments=dict(zip("ATCGNK", "TAGCNS"))):
    return "".join(compliments[c] for c in reversed(kmer))


def df_kmer_counts(proms, kmers):
    """ given fasta like data in a dataframe, proms, and a set of kmers to count,
        return a dataframe stating for each promoter how many of each canonical
        kmer occurs
    """
    rckmers = set(kmers) | {reverse_comp(k) for k in kmers}
    counts = {}
    lens = set(len(k) for k in kmers)
    for l in lens:
        for g, seq in zip(proms.ID, proms.SEQ):
            iters = [seq[i:] for i in range(l)]
            g_kmers = ["".join(k) for k in zip(*iters)]
            counted_kmers = Counter(k for k in g_kmers if(k in rckmers))
            counts[g] = counts.get(g, Counter()) + counted_kmers
    counts = pandas.DataFrame(counts)
    return counts.groupby(
        [k if k in kmers else reverse_comp(k) for k in counts.index]
    ).sum().transpose()


def silo_kmeans(df, tests=range(3, 30), **args):
    """ fit a gaussian mixture model using silhouette scores on df
        and return the best model
    """
    rets = []
    for i in tests:
        m = KMeans(n_clusters=i, **args)
        c = m.fit_predict(df)
        score = silhouette_score(df, c)
        rets.append((score, m))
    score, m = max(rets)
    return m


def chi_kmeans(df, gene_sets, tests=range(3, 30), **args):
    """ fit a gaussian mixture model that optimized the dependenace on the 
        clusters in gene_sets and return the best model
    """
    rets = []
    de_map = df.index.map(lambda g: gene_sets.get(g, "cluster_NULL"))
    for i in tests:
        m = KMeans(n_clusters=i, **args)
        c = pandas.Series(m.fit_predict(df), index=df.index)
        t = pandas.DataFrame({"promoters": c, "markers": de_map}).dropna()
        cross_df = t.reset_index().groupby(["promoters", "markers"]).count().iloc[:, 0].unstack().fillna(0)
        chi2, p, dof, ex = chi2_contingency(cross_df, correction=True)
        rets.append((p, m))
    adjusted_p = min(multipletests([p for p, _ in rets], method="fdr_bh")[1])
    p, m = min(rets, key=lambda e: e[0])
    return adjusted_p, m


def main():
    parser = argparse.ArgumentParser(description='A set of tools for promoter sequence analysis : ')
    parser.set_defaults(which="help")
    subparsers = parser.add_subparsers()

    arg_descriptions = {
        'promoters': (["promoters"], {"help": "a fasta file containing promoter region sequences"}),
        'features': (["features"], {"help": "the output from 'promoter_analysis.py features'"}),
        'reducer': (["reducer"], {"help":"the output from 'promoter_analysis.py train'"}),
        "reduced": (["reduced"], {"help": "The otuput from 'promoter_analysis transform'"}),
        'classifier': (["classifier"], {"help": "The output from 'promoter_analysis classifier'"}),
        "clusters": (["clusters"], {"help": "output from 'promoter_analysis cluster'"}),
        'kmers': (["-k", "--kmers"], {"help": "a file containing canonical kmers to use for generating features", "default": hexmers_path}),
        "gene_ids": (["-i", "--gene_ids"], {"help": "a set of gene ids to highlight in the plot", "nargs": "*", "default": []}),
        'train_genes': (["-t", "--train_genes"], {"help": "a file containing gene ids to be used for training. Default is random 10K genes"}),
        'algorithm': (["-a", "--algorithm"], {"help": "which normalization/reduction pipeline to use. Allowed values: 0", "default": 0}),
        "gene_cats": (['-g', "--gene_cats"], {"help": "a two column tsv: gene id, category. with labels for some genes of interest (DE genes for example)"}),
        "outbase": (["-o", "--outbase"], {'help': 'prefix for output files. Default is "promoters"', "default": "promoters"})
    }

    sub_commands = {
        "all": (
            "runs each step in sequence",
            do_all,
            ['promoters', 'kmers', 'gene_cats', 'train_genes', 'algorithm', 'gene_ids', 'outbase'],
        ),
        "features": (
            "extracts features for some promoter set",
            do_features,
            ['promoters', 'kmers', 'outbase'],
        ),
        "reducer": (
            "trains an embedding model for some feature set",
            do_reducer,
            ['features', 'algorithm', 'train_genes', 'outbase'],
        ),
        "transform": (
            "applies a model to some feature set",
            do_transform,
            ['features', 'reducer', 'outbase'],
        ),
        "classifier": (
            "learns a classifier for promoters using optional gene categories for hyperparameter optimization",
            do_classifier,
            ['reduced', 'gene_cats', 'outbase'],
        ),
        "cluster": (
            "applies a classifier to promoters",
            do_cluster,
            ['reduced', 'classifier', 'outbase']
        ),
        "plot": (
            "visualizes a clustered embedding",
            do_plotting,
            ["clusters", "gene_ids", "gene_cats", "outbase"]
        )
    }

    for k, (h, f, arg_set) in sub_commands.items():
        sub = subparsers.add_parser(k, help=h)
        for a in arg_set:
            positionals, keywords = arg_descriptions[a]
            sub.add_argument(*positionals, **keywords)
        sub.set_defaults(which=k)

    args = parser.parse_args()
    if(args.which == 'help'):
        parser.print_help()
    for k, (h, f, arg_set) in sub_commands.items():
        if(args.which == k):
            f(**{a: args.__dict__.get(a) for a in arg_set})


def do_features(promoters, kmers, outbase):
    promoters = load_fasta(promoters)
    kmers = set(load_list(kmers))
    features = df_kmer_counts(promoters, kmers)
    outpath = outbase + ".features.tsv"
    features.to_csv(outpath, sep="\t")
    return outpath


def do_reducer(features, algorithm, train_genes, outbase):
    features = pandas.read_csv(features, sep="\t", index_col=0)
    promoter_ids = set(features.index)
    train_genes = random.sample(promoter_ids, k=10000) if(train_genes is None) else load_list(train_genes)
    bad_genes = [g for g in train_genes if(g not in promoter_ids)]
    train_genes = [g for g in train_genes if(g in promoter_ids)]
    if(len(bad_genes) > 0):
        warnings.warn(f"WARNING: The following genes don't match promoters:\n" + '\n'.join(bad_genes))
    pipe = [
        Pipeline([('normalizer', Normalizer()), ('scaler', StandardScaler()), ('isomap', Isomap())])
    ][algorithm]
    pipe.fit(features.loc[train_genes])
    outpath = outbase + ".reducer.joblib"
    joblib.dump(pipe, outpath)
    return outpath


def do_transform(features, reducer, outbase):
    features = pandas.read_csv(features, sep="\t", index_col=0)
    pipe = joblib.load(reducer)
    reduced = pandas.DataFrame(
        pipe.transform(features),
        index=features.index,
        columns=['pc1', 'pc2']
    )
    outpath = outbase + ".reduced.tsv"
    reduced.to_csv(outpath, sep="\t")
    return outpath


def do_classifier(reduced, gene_cats, outbase):
    reduced = pandas.read_csv(reduced, sep="\t", index_col=0)
    if(gene_cats is not None):
        gene_cats = pandas.read_csv(gene_cats, sep="\t", names=["geneID", "label"])
        bad_genes = [g for g in gene_cats.geneID if(g not in reduced.index)]
        if(len(bad_genes) > 0):
            msg = f"WARNING: Some genes don't match promoters: {len(bad_genes)} :\n"
            if(len(bad_genes) < 100):
                msg += "\n".join(bad_genes)
            warnings.warn(msg)
        gene_counts = gene_cats.geneID.value_counts()
        multi_genes = gene_counts[gene_counts != 1]
        if(len(multi_genes) > 0):
            msg = f"WARNING : Some genes are associated with multiple categories: {len(multi_genes)} :\n"
            if(len(multi_genes) < 100):
                msg += "\n".join(multi_genes.index)
            warnings.warn(msg)
        gene_cats = gene_cats[~gene_cats.geneID.isin(multi_genes.index)]
        gene_cats = dict(zip(gene_cats.geneID, gene_cats.label))
        adjusted_p, classifier = chi_kmeans(reduced, gene_cats)
        sys.stderr.write("CHI_GMM best adjusted p value: " + str(adjusted_p) + "\n")
    else:
        classifier = silo_kmeans(reduced)
    outpath = outbase + ".classifier.joblib"
    joblib.dump(classifier, outpath)
    return outpath


def do_cluster(reduced, classifier, outbase):
    reduced = pandas.read_csv(reduced, sep="\t", index_col=0)
    classifier = joblib.load(classifier)
    reduced["cluster"] = classifier.predict(reduced)
    outpath = outbase + ".clusters.tsv"
    reduced.to_csv(outpath, sep="\t")
    return outpath


##### TODO #####
""" add gene_cat support to visualization. If present, draw heatmap with toggle to switch between
    log(-log(significance)) and FC, along with a scatterplot that permits highlighting specific genes
    via a menu of some kind. Likely will require switching to a template html that gets populated as
    appropriate 
"""
def do_plotting(clusters, gene_ids, gene_cats, outbase):
    clusters = pandas.read_csv(clusters, sep="\t", index_col=0)
    genes_data = dict(zip(clusters.index, zip(clusters.pc1, clusters.pc2, clusters.cluster)))
    bad_genes = [g for g in gene_ids if g not in genes_data]
    good_genes = [g for g in gene_ids if g in genes_data]
    if(len(bad_genes) > 0):
        warnings.warn("The following genes don't match promoters:\n" + "\n".join(bad_genes))
    with open(plot_template) as fin:
        template = Template(fin.read())
    htmltext = template.substitute(
        genes_data=json.dumps(genes_data),
        outbase=outbase,
        defgenes=json.dumps(good_genes)
    )
    outpath = outbase + ".html"
    with open(outpath, "w") as fout:
        fout.write(htmltext)
    return outpath


def do_all(promoters, kmers, gene_cats, train_genes, algorithm, gene_ids, outbase):
    sys.stderr.write("Computing features\n")
    features = do_features(promoters, kmers, outbase)
    sys.stderr.write("Learning Reducer\n")
    model = do_reducer(features, algorithm, train_genes, outbase)
    sys.stderr.write("Applying Reducer\n")
    reduced = do_transform(features, model, outbase)
    sys.stderr.write("Learning Classifier\n")
    classifier = do_classifier(reduced, gene_cats, outbase)
    sys.stderr.write("Applying Classifier\n")
    clusters = do_cluster(reduced, classifier, outbase)
    sys.stderr.write("Generating Figure\n")
    plot = do_plotting(clusters, gene_ids, gene_cats, outbase)


if(__name__ == "__main__"):
    main()
