#!/usr/bin/env python3

import argparse
import os
import sys
from collections import deque
import pandas
from BCBio import GFF
from Bio import SeqIO, Seq
from Bio.SeqRecord import SeqRecord


def load_data(fasta_path, gff3_path):
    with open(fasta_path) as fin:
        seq_dict = SeqIO.to_dict(SeqIO.parse(fin, "fasta"))
    with open(gff3_path) as fin:
        return list(GFF.parse(fin, base_dict=seq_dict))


def get_promoters(data, promoter_length):
    prom_records = []
    for rec in data:
        features = deque(f for f in rec.features)
        genes = deque([])
        while len(features) > 0:
            f = features.popleft()
            features.extend(f.sub_features)
            if(f.type == "gene"):
                genes.append(f)
        cds = {}
        for g in genes:
            features = deque(g.sub_features)
            while(len(features) > 0):
                f = features.popleft()
                features.extend(f.sub_features)
                if(f.type == "CDS"):
                    temp = cds.get(g.id, [])
                    temp.append(f)
                    cds[g.id] = temp
        for gid, feats in cds.items():
            if(feats[0].strand == -1):
                pos = max(f.location.nofuzzy_end for f in feats)
                seq = rec.seq[pos: pos + promoter_length].reverse_complement()
            else:
                pos = min(f.location.nofuzzy_start for f in feats)
                seq = rec.seq[pos - promoter_length : pos]
            prom_records.append(SeqRecord(seq, gid, "", ""))
    return prom_records


def extract_promoters(fasta_path, gff3_path, promoter_length):
    """ generates all needed sequences for crops bioinformatics analysis

        input:
            fasta_path : path to fasta file containing genome
            gff3_path : path to gff3 file containing gene models
            promoter_length : how big of a promoter region to extract
    """
    data = load_data(fasta_path, gff3_path)
    promoters = get_promoters(data, promoter_length)
    SeqIO.write(promoters, sys.stdout, "fasta")


def cli_main():
    parser = argparse.ArgumentParser(
        description=(
            "a tool that extracts promoter regions from a fasta file based on a provided gff3. Fasta will be written to stdout"
        )
    )
    parser.add_argument(
        "fasta",
        help="fasta file matching the gff3"
    )
    parser.add_argument(
        "gff3",
        help="gff3 containing features to use"
    )
    parser.add_argument(
        "--promoter_length",
        default=500,
        type=int,
        help="default length for promoter region to extract"
    )
    args = parser.parse_args()
    extract_promoters(
        args.fasta,
        args.gff3,
        args.promoter_length,
    )



if(__name__ == "__main__"):
    cli_main()
