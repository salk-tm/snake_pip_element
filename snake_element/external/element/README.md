# Element Motif Finder
A C++ tool that searches across sets of sequences for overrepresented motifs.

Authors: Doug Bryant, Connor McEntee  
Organization: Mockler Lab, Donald Danforth Plant Science Center, St. Louis, MO USA  
Date: 6/14/12  

## Introduction

Element identifies overrepresented motifs in a set of input sequences by exhaustively comparing the occurrences of each motif against
what is expected based on a set of background statistics.  This package contains the source code and set of example files to get you
up and running using element.

- README.md: the file you are reading now
- LICENSE: a file describing the license under which Element is released
- src/: a directory containing the source code
- examples/:  a directory containing examples of inputs and outputs
- paper/: a directory containing the Element publication

## Installing Element

### Prerequisites

Element depends upon several C++ libraries that need to be present when it is compiled.  Please ensure that these are installed
on your system before attempting to use Element:

- Boost: http://www.boost.org/
- SeqAn: http://www.seqan.de/

### Installing From Source

Element can be installed by compiling it from the source files included in this package.  Ensure that you have an updated C++
compiler that supports OpenMP, which Element needs for multithreading.

To download the source files run either

    git clone git@github.com:mocklerlab/element.git
    
if you have git installed or 

    curl -L -o element.tar.gz https://api.github.com/repos/mocklerlab/element/tarball
    mkdir element && tar -zxvf element.tar.gz -C element --strip-components 1

Once the files are extracted, you can compile element from source.  Unless the SeqAn header files are in the standard system
include directory `/usr/incude/seqan` you will need to explicitly specify the path when you generate the Makefile

    cd element
    ./configure --with-seqan-include-path PATH_TO_SEQAN
    make
    sudo make install

## Using Element

A complete run of element is broken down into three steps invoked by separate commands:

- bground: Generates the background statistics file
- count: Calculates the raw counts and resultant statistics for motifs against the input sequence as compared to the background
- filter: Identifies the motifs that are significant
- cluster: Aggregates the significant results into a hierarchical tree based on similarity 
- cut: Cuts the cluster tree into a set of discrete clusters

### Generating Background Statistics

Before Element can be used to identify overrepresented motifs, background statistics need to be generated.  Typically, the
background is based on entire genome, though there is certainly no restriction on this (However, it is __strongly__ recommended that
all of the input sequences have the same length, since is this is assumed in the subsequent statistical calculations).  The 
following example demonstrates how to generate the background statistics for the entire set of 500bp promoters for the 
_Arabidopsis thaliana_ genome.  

    element bground -w examples/words.txt -s examples/Arabidopsis_thaliana_promoter_500.fa \
    -o examples/Arabidopsis_thaliana_promoter_500.bground    

Since this is computationally expensive, it can optionally be threaded using the `-t` flag

    element bground -w examples/words.txt -s examples/Arabidopsis_thaliana_promoter_500.fa \
    -o examples/Arabidopsis_thaliana_promoter_500.bground -t 4    

This creates a human readable background statistics file for which the first few lines will be:

    AAC     672890  20.0265 4.88242 0.99997  33599
    AAT     1197017 35.6255 10.2875 1        33600
    ACA     646233  19.2331 5.5338  0.99994  33598
    ACC     318287  9.47283 3.94541 0.999375 33579

The columns are:

- Motif: The kmer that was searched
- Total Hits: The total number of times that the motif appears on both the forward and reverse strands over all the input sequences
- Mean Hits: The average number of times that motif appears in a sequence 
- Hit Standard Deviation: The standard deviation for the number of times a motif appears in a sequence. (This is not the standard 
deviation of the mean.) 
- Hit Probability: The probability that a randomly selected sequence contains at least one occurrence of the motif
- Hit Sequences: The number of sequences that have at least one occurrence of the motif

### Counting Motifs ###

With a set of background statistics in hand, the next step to identifying overrepresented motifs in a set of input sequences is to
count all of the occurrences of the words in them.  The following example illustrates how to use the background statistics generated
above to identify overrepresented motifs across a set of diurnally expressed genes:

    element count -w examples/words.txt -o examples/Arabidopsis_diurnal_sequences.fa.element \
    examples/Arabidopsis_diurnal_sequences.fa examples/Arabidopsis_thaliana_promoter_500.bground

This command can also be multithreaded using the `-t` flag

    element count -t 4 -w examples/words.txt -o examples/Arabidopsis_diurnal_sequences.fa.element \
    examples/Arabidopsis_diurnal_sequences.fa examples/Arabidopsis_thaliana_promoter_500.bground

The produces a file almost similar to the what the background statistics calculation produces.  However, all of the 
counts are only totaled over set the of input sequences and not the entire background, and additional p-values for test statistics
are calculated.

The columns are:

- Motif: The kmer that was searched
- Total Hits: The total number of times that the motif appears on both the forward and reverse strands over all the input sequences
- Mean Hits: The average number of times that motif appears in a sequence 
- Hit Standard Deviation: The standard deviation for the number of times a motif appears in a sequence. (This is not the standard 
deviation of the mean.) 
- Hit Probability: The probability that a randomly selected sequence contains at least one occurrence of the motif
- Hit Sequences: The number of sequences that have at least one occurrence of the motif
- Hit P-value: The p-value for the number of hit sequences calculated using a binomial test statistic
- Mean P-value: The p-value for the average number of motifs per sequence calculated using a Poisson test statistic

### Filtering Counts ###

In order to get meaningful results the raw counts generated by element need to be filtered into a smaller set.  Element provides several
approaches to filtering the counts, though for most cases the default method should be suitable.  The following example illustrates
the default approach

    element filter -c examples/Arabidopsis_diurnal_sequences.fa.element
    
This imposes the standard cutoff of a 0.05 p-value correcting for multiple testing using the Benjamini-Hochberg filter.  A more 
stringent cutoff could be imposed via the `-a` flag e.g.

    element filter -c examples/Arabidopsis_diurnal_sequences.fa.element -a 0.01
    
The format of the resultant file is identical to the counting step, except the number of results is reduced.  By default, the
filtering is applied based on the Hit Sequences test statistic, but it can also be applied to the mean.

Several different standard filtering methods are available which account for false positives due to multiple hypothesis testing:

- Benjamini-Hochberg: Controls the false discovery rate.
- Bonferroni Correction: Conservative method for controlling the familywise error rate.
- Bonferroni-Holm: Improved method for controlling the familywise error rate.
- Fixed Cutoff: No correction is applied to the p-values, in which results with p-values below the cutoff are retained. 

### Clustering Results ###
    
In many cases, the number of significant results can be large with many of the significant motifs mostly overlapping.  Element helps
summarize the results by clustering similar motifs together via sequence similarity.

    element cluster -i examples/Arabidopsis_diurnal_sequences.fa.element.filtered

This produces an xml file representing the hierarchical clustering of all of the motifs in the input set.  Two parameters that
affect the clustering can be set: the metric that determines how similar two words are and the clustering algorithm.  In almost all
cases the defaults will give the best results.

Distance Metric (`-m` flag):

- Levenshtien: Penalizes all differences between motifs equally
- Smith-Waterman: Vanilla SW alignment that allows for gaps
- Modified Smith-Watermam: Adjusts the SW scoring scheme to better compensate for smaller motifs contained in larger motifs (Default)

Clustering Algorithm (`-c` flag):

- Single-linkage: The distance between clusters is determined by the closest distance between one member of each.  This results in
fewer clusters with more dissimilarity that chain together.
- Complete-linkage: The distance between clusters is determined by the greatest distance between any two members. (Default)
- Weighted-linkage: The weighted average distance (WPGMA).
- Median-linkage: The weighted center of mass distance (WPGMC).

### Cutting into Clusters ###

The XML cluster tree is efficient for representing the hierarchical structure for a computer to parse.  However, it is not human
readable.  Instead, the cluster tree can be separated into discrete clusters based on cutoff that sets the maximum allowable distance
between any two elements in the same cluster.  For example,

    element cut -h 5.0 -c examples/Arabidopsis_diurnal_sequences.fa.element.filtered.xml

separate the 15 results into 4 distinct clusters.  A quick search through a promoter motif database shows that all four of these 
clusters indeed match known regulatory motifs.
