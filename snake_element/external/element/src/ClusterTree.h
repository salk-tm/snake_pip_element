#ifndef CLUSTERTREE_H
#define	CLUSTERTREE_H

#include <iostream>
#include <stdint.h>
#include <string>
#include <vector>
#include <math.h>
#include "pugixml.hpp"
#include "WordStats.h"

typedef int_fast32_t t_index;

struct node {
  t_index node1, node2;
  double distance;

  inline friend bool operator< (const node a, const node b) {
    return a.distance < b.distance || (a.distance == a.distance && b.distance != b.distance);
  }
};

template <typename type>
class auto_array_ptr{
private:
    type* ptr;
public:
    auto_array_ptr() { ptr = NULL; }
    template <typename index>
    auto_array_ptr(index const size) { init(size); }
    template <typename index, typename value>
    auto_array_ptr(index const size, value const val) { init(size, val); }
    ~auto_array_ptr() { delete [] ptr; }
    void free() {
        delete [] ptr;
        ptr = NULL;
    }
    template <typename index>
    void init(index const size) {
        ptr = new type [size];
    }
    template <typename index, typename value>
    void init(index const size, value const val) {
        init(size);
        for (index i=0; i<size; i++) ptr[i] = val;
    }
    inline operator type *() const { return ptr; }
};

class ClusterTree {
public:
    ClusterTree(std::vector<WordStatsObserved* >* p_vMembers);
    virtual ~ClusterTree();
    void append(const t_index node1, const t_index node2, const double p_nDistance);
    node* at(int p_iIndex);
    bool toXML(const std::string p_sFileName);
    int size() { return m_vNodes->size(); }
private:
    bool addXMLNode(pugi::xml_node p_dNode, unsigned int p_iIndex);
    bool addXMLChildNode(pugi::xml_node p_dNode, t_index p_iNodeIndex);
    std::vector<WordStatsObserved* >*   m_vMembers;
    std::vector<node >*                 m_vNodes;
};

#endif	/* CLUSTERTREE_H */