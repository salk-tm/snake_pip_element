#include "WordMetric.h"

double WordMetric::LevenshtienDistance(const std::string p_sSourceWord, const std::string p_sTargetWord) {
    const unsigned int l_iSourceLength = p_sSourceWord.length();
    const unsigned int l_iTargetLength = p_sTargetWord.length();
    boost::numeric::ublas::matrix<int> l_dMatrix(l_iSourceLength + 1, l_iTargetLength + 1);
    for (unsigned int i = 0; i < l_iSourceLength + 1; i++) {
        l_dMatrix(i, 0) = i;
    }
    for (unsigned int i = 0; i < l_iTargetLength + 1; i++) {
        l_dMatrix(0, i) = i;
    }

    for (unsigned int i = 0; i < l_iSourceLength; i++) {
        for (unsigned int j = 0; j < l_iTargetLength; j++) {
            int l_iCellCost = 1;
            if (p_sTargetWord[j] == p_sSourceWord[i]) {
                l_iCellCost = 0;
            }
            int l_iMinCost = l_dMatrix(i, j);
            if (l_iMinCost > l_dMatrix(i, j + 1)) {
                l_iMinCost = l_dMatrix(i, j + 1);
            }
            if (l_iMinCost > l_dMatrix(i + 1, j)) {
                l_iMinCost = l_dMatrix(i + 1, j);
            }
            l_iCellCost += l_iMinCost;
            l_dMatrix(i + 1, j + 1) = l_iCellCost;
        }
    }

    return (double) l_dMatrix(l_iSourceLength, l_iTargetLength);
}

double WordMetric::SmithWatermanDistance(const std::string p_sSourceWord, const std::string p_sTargetWord) {
    double l_nScore = WordMetric::SmithWatermanDistance(p_sSourceWord, p_sTargetWord, seqan::Score<int>(1, -1, -1, -1));

    return l_nScore;
}

double WordMetric::ModifiedSmithWatermanDistance(const std::string p_sSourceWord, const std::string p_sTargetWord) {
    int l_iDifference = p_sSourceWord.length() - p_sTargetWord.length();
    std::string l_sLongerWord;
    std::string l_sShorterWord;
    if (l_iDifference > 0) {
        l_sLongerWord = p_sSourceWord;
        l_sShorterWord = p_sTargetWord;
    } else {
        l_sLongerWord = p_sTargetWord;
        l_sShorterWord = p_sSourceWord;
        l_iDifference *= -1;
    }
    std::vector<double> l_vScores;
    for(unsigned int i = 0; i <= l_iDifference; i++) {
        std::string l_sTruncatedWord = l_sLongerWord.substr(i, l_sShorterWord.length());
        double l_nScore = WordMetric::SmithWatermanDistance(l_sTruncatedWord, l_sShorterWord, seqan::Score<int>(1, -1, -10));
        l_vScores.push_back(l_nScore);
    }
    l_vScores.push_back(WordMetric::SmithWatermanDistance(p_sSourceWord, p_sTargetWord));
    
    double l_vBestScore = *std::min_element(l_vScores.begin(), l_vScores.end());
    return l_vBestScore;
}

double WordMetric::SmithWatermanDistance(const std::string p_sSourceWord, const std::string p_sTargetWord, seqan::Score<int> p_dScore) {
    seqan::String<char> l_dSourceSequence = p_sSourceWord;
    seqan::String<char> l_dTargetSequence = p_sTargetWord;
    
    seqan::Align<seqan::String<char>, seqan::ArrayGaps> l_dForwardAlign;
    seqan::resize(seqan::rows(l_dForwardAlign), 2);
    seqan::assignSource(seqan::row(l_dForwardAlign, 0), l_dSourceSequence);
    seqan::assignSource(seqan::row(l_dForwardAlign, 1), l_dTargetSequence);
    int l_iForwardScore = seqan::globalAlignment(l_dForwardAlign, p_dScore, seqan::Gotoh());

    seqan::Align<seqan::String<char>, seqan::ArrayGaps> l_dReverseAlign;
    seqan::resize(seqan::rows(l_dReverseAlign), 2);
    seqan::String<char> l_dReverseComplementSequence = __utility::findReverseComplement(p_sTargetWord);
    seqan::assignSource(seqan::row(l_dReverseAlign, 0), l_dSourceSequence);
    seqan::assignSource(seqan::row(l_dReverseAlign, 1), l_dReverseComplementSequence);
    int l_iReverseScore = seqan::globalAlignment(l_dReverseAlign, p_dScore, seqan::Gotoh());
    
    int l_iBestScore;
    if (l_iForwardScore >= l_iReverseScore) {
        l_iBestScore = l_iForwardScore;
    } else {
        l_iBestScore = l_iReverseScore;
    }

    int l_iMaxScore;
    double l_nRatio;
    if (p_sSourceWord.length() >= p_sTargetWord.length()) {
        l_iMaxScore = p_sSourceWord.length();
        l_nRatio = p_sTargetWord.length();
    } else {
        l_iMaxScore = p_sTargetWord.length();
        l_nRatio = p_sSourceWord.length();
    }
    l_nRatio /= l_iMaxScore;

    return (double) (l_iMaxScore - l_iBestScore) * l_nRatio;
}
