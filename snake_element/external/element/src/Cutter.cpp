#include "Cutter.h"

Cutter::Cutter(std::string* p_sInputFileName) {
    m_dXMLDocumentTree = new pugi::xml_document();
    this->loadClusterFile(p_sInputFileName);
}

Cutter::~Cutter() {
    delete m_dXMLDocumentTree;
}

bool Cutter::loadClusterFile(std::string* p_sInputFileName) {
    bool l_bReturnStatus = true;
    pugi::xml_parse_result l_bParseResult = m_dXMLDocumentTree->load_file(p_sInputFileName->c_str());
    if (!l_bParseResult) {
        std::cout << "XML [" << p_sInputFileName << "] parsed with errors";
        std::cout << "Error description: " << l_bParseResult.description() << std::endl;
        std::cout << "Error offset: " << l_bParseResult.offset << " (error at [...";
        std::cout << (p_sInputFileName + l_bParseResult.offset) << "]" << std::endl << std::endl;
        l_bReturnStatus = false;
    }
    
    return l_bReturnStatus;
}

std::vector<std::string> Cutter::getClustersAsStrings(double* p_nCutoff) {
    std::vector<std::string> l_vResults;
    std::vector<std::vector<WordStatsObserved* >* >* l_vClusters = this->getClusters(p_nCutoff);
    for (unsigned int i = 0; i < l_vClusters->size(); i++) {
        std::vector<WordStatsObserved* >* l_vCluster = l_vClusters->at(i);
        std::stringstream l_dStringStream;
        l_dStringStream.precision(6);
        l_dStringStream << "### Cluster #" <<  i + 1 << " ###" << std::endl;
        for(unsigned int j = 0; j < l_vCluster->size(); j++) {
            WordStatsObserved* l_dWordStatsObserved = l_vCluster->at(j);
            l_dStringStream << l_dWordStatsObserved->toString() << std::endl;
        }
        l_vResults.push_back(l_dStringStream.str());
    }
    
    return l_vResults;
}

std::vector<std::vector<WordStatsObserved* >* >* Cutter::getClusters(double* p_nCutoff) {
    std::vector<std::vector<WordStatsObserved* >* >* l_vCutClusters = new std::vector<std::vector<WordStatsObserved* >* >();
    pugi::xml_node l_dRootNode = m_dXMLDocumentTree->child("clustered_results");
    l_vCutClusters = this->cutNode(l_dRootNode.first_child(), p_nCutoff);
    return l_vCutClusters;
}

std::vector<std::vector<WordStatsObserved* >* >* Cutter::cutNode(pugi::xml_node p_dNode, double* p_nCutoff) {    
    std::vector<std::vector<WordStatsObserved* >* >* l_vResults = new std::vector<std::vector<WordStatsObserved* >* >();
    pugi::xml_node l_dNodes = p_dNode.child("nodes");
    if (l_dNodes) {
        if (l_dNodes.attribute("distance").as_double() >= *p_nCutoff) {
            // Traverse both nodes attempting to cut them as well.
            pugi::xml_node l_dChildNode = l_dNodes.first_child();
            std::vector<std::vector<WordStatsObserved* >* >* l_vChildClusters = this->cutNode(l_dChildNode, p_nCutoff); 
            l_vResults->insert(l_vResults->end(), l_vChildClusters->begin(), l_vChildClusters->end());
            
            l_dChildNode = l_dNodes.last_child();
            l_vChildClusters = this->cutNode(l_dChildNode, p_nCutoff); 
            l_vResults->insert(l_vResults->end(), l_vChildClusters->begin(), l_vChildClusters->end());
        } else {
            // Pull out all of the children, and treat as one cluster
            std::vector<WordStatsObserved* >* l_vTempResults = new std::vector<WordStatsObserved*>();
            pugi::xpath_node_set l_dResults = p_dNode.select_nodes("descendant::result");
            for (pugi::xpath_node_set::const_iterator l_dResultIterator = l_dResults.begin(); l_dResultIterator != l_dResults.end(); l_dResultIterator++) {
                pugi::xml_node l_dResultNode = l_dResultIterator->node();
                WordStatsObserved* l_dTempWordStats = new WordStatsObserved(this->nodeToWordStat(l_dResultNode));
                l_vTempResults->push_back(l_dTempWordStats);
std::cout << l_dTempWordStats->getWord() << std::endl;         
}
std::cout << "###" << std::endl;
            l_vResults->push_back(l_vTempResults);
        }
    } else {
        std::vector<WordStatsObserved* >* l_vTempResults = new std::vector<WordStatsObserved* >();
        WordStatsObserved* l_dTempWordStatsObserved = new WordStatsObserved(this->nodeToWordStat(p_dNode));
        l_vTempResults->push_back(l_dTempWordStatsObserved);
        l_vResults->push_back(l_vTempResults);
    }
    
    return l_vResults;
}

WordStatsObserved Cutter::nodeToWordStat(pugi::xml_node p_dNode) {
    std::string* l_sMotif   = new std::string(p_dNode.attribute("motif").as_string());
    int l_iCount            = p_dNode.attribute("total_hits").as_int();
    double l_nMean          = p_dNode.attribute("mean_hits").as_double();
    double l_nStddev        = p_dNode.attribute("hit_stddev").as_double();
    double l_nProb          = p_dNode.attribute("hit_prob").as_double();
    int l_iHitSeqs          = p_dNode.attribute("hit_seq").as_int();
    double l_nHitPValue     = p_dNode.attribute("hit_p_value").as_double();
    double l_nMeanPValue    = p_dNode.attribute("mean_p_value").as_double();
    
    WordStatsObserved* l_dTempWordStatsObserved = new WordStatsObserved
        (*l_sMotif, l_iCount, l_nMean, l_nStddev, l_nProb, l_iHitSeqs, l_nHitPValue, l_nMeanPValue);
        
    return *l_dTempWordStatsObserved;
}
