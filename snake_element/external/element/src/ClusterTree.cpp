#include "ClusterTree.h"

ClusterTree::ClusterTree(std::vector<WordStatsObserved* >* p_vMembers) {
    m_vMembers = p_vMembers;
    m_vNodes = new std::vector<node >();
}            

ClusterTree::~ClusterTree() {
    delete m_vNodes;
}

void ClusterTree::append(const t_index p_dNode1, const t_index p_dNode2, const double p_nDistance) {
    node l_dNode;
    l_dNode.node1 = p_dNode1;
    l_dNode.node2 = p_dNode2;
    l_dNode.distance = p_nDistance;
    m_vNodes->push_back(l_dNode);
}

node* ClusterTree::at(int p_iIndex) {
    return &(m_vNodes->at(p_iIndex));
}

bool ClusterTree::toXML(const std::string p_sFileName) {
    pugi::xml_document l_dDocument;
    pugi::xml_node l_dRootNode = l_dDocument.append_child("clustered_results");
    if (m_vMembers->size() > 0) {
        this->addXMLNode(l_dRootNode, this->size() - 1);
    }
    l_dDocument.save_file(p_sFileName.c_str(), "  ");
    
    return true;
}

bool ClusterTree::addXMLNode(pugi::xml_node p_dNode, unsigned int p_iIndex) {
    pugi::xml_node l_dContainerNode = p_dNode.append_child("node");
    pugi::xml_node l_dChildNodes = l_dContainerNode.append_child("nodes");
    
    node* l_dNode = this->at(p_iIndex);
    l_dChildNodes.append_attribute("distance") = l_dNode->distance;
    this->addXMLChildNode(l_dChildNodes, l_dNode->node1);
    if (l_dNode->node2 != l_dNode->node1) {
        this->addXMLChildNode(l_dChildNodes, l_dNode->node2);
    }
    
    return true;
}

bool ClusterTree::addXMLChildNode(pugi::xml_node p_dNode, t_index p_iNodeIndex) {
    if(p_iNodeIndex >= m_vMembers->size()) {
        this->addXMLNode(p_dNode, p_iNodeIndex - m_vMembers->size());
    } else {
        WordStatsObserved* l_dWordStats = m_vMembers->at(p_iNodeIndex);
        pugi::xml_node l_dResultNode = p_dNode.append_child("result");
        l_dResultNode.append_attribute("motif")         = l_dWordStats->getWord().c_str();
        l_dResultNode.append_attribute("total_hits")    = l_dWordStats->getCount();
        l_dResultNode.append_attribute("mean_hits")     = l_dWordStats->getMean();
        l_dResultNode.append_attribute("hit_stddev")    = l_dWordStats->getStddev();
        l_dResultNode.append_attribute("hit_prob")      = l_dWordStats->getProb();
        l_dResultNode.append_attribute("hit_seq")       = l_dWordStats->getHitSeqs();
        l_dResultNode.append_attribute("hit_p_value")   = l_dWordStats->getHitPValue();
        l_dResultNode.append_attribute("mean_p_value")  = l_dWordStats->getMeanPValue();
    }
    return true;
}