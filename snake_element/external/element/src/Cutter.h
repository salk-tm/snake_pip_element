#ifndef CUTTER_H
#define	CUTTER_H

#include <vector>
#include <iostream>
#include "Parameters.h"
#include "WordStats.h"
#include "pugixml.hpp"

class Cutter {
public:
    Cutter(std::string*);
    virtual ~Cutter();
    std::vector<std::vector<WordStatsObserved* >* >* getClusters(double*);
    std::vector<std::string> getClustersAsStrings(double*);
private:
    bool loadClusterFile(std::string*);
    std::vector<std::vector<WordStatsObserved* >* >* cutNode(pugi::xml_node, double*);
    WordStatsObserved nodeToWordStat(pugi::xml_node);
    pugi::xml_document* m_dXMLDocumentTree;
};

#endif	/* CUTTER_H */
