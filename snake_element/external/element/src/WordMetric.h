#ifndef WORDMETRIC_H
#define	WORDMETRIC_H

#include <string>
#include <seqan/align.h>
#include <boost/numeric/ublas/matrix.hpp>
#include "stdafx.h"

class WordMetric {
public:
    static double LevenshtienDistance(const std::string, const std::string);
    static double SmithWatermanDistance(const std::string, const std::string);
    static double ModifiedSmithWatermanDistance(const std::string, const std::string);
private:
    static double SmithWatermanDistance(const std::string, const std::string, seqan::Score<int>);
};

#endif	/* WORDMETRIC_H */