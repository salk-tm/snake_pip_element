#ifndef CUT_H
#define	CUT_H

#include <fstream>
#include <stdlib.h>
#include "Parameters.h"
#include "Cutter.h"

class Cut {
public:
    Cut(int, char**);
    virtual ~Cut();
private:
    void outputUsage();
    bool run();
    CutParameters*   m_dCutParameters;
};

#endif	/* CUT_H */
