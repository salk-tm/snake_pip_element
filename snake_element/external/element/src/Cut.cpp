#include "Cut.h"

Cut::Cut(int argc, char** argv) {
    m_dCutParameters = new CutParameters();
    if(argc < 2) { Cut::outputUsage(); return; }
    else {
        char* l_cParameter;
        for(int i = 1; i < argc; i++) {
            l_cParameter = argv[i];
            if(l_cParameter[0] == '-') {
                switch(l_cParameter[1]) {
                    case 'o': m_dCutParameters->setOutputFileName(argv[++i]); break;
                    case 'c': m_dCutParameters->setInputCountFileName(argv[++i]); break;
                    case 'k': m_dCutParameters->setClusterCount(atoi(argv[++i])); break;
                    case 'h': m_dCutParameters->setCutoff(atof(argv[++i])); break;
                    default: std::cout << "Error in " << argv[i] << " " << argv[++i] << std::endl;
                }
            }
        }
    }    
    this->run();
}

Cut::~Cut() {
    delete m_dCutParameters;
}

bool Cut::run() {
    bool l_bSuccess = true;
    if(m_dCutParameters->verifyParameters()) {
        Cutter* l_dCutter = new Cutter(m_dCutParameters->getInputCountFileName());
        std::vector<std::string> l_vResults = l_dCutter->getClustersAsStrings(m_dCutParameters->getCutoff());
        if(l_vResults.size() != 0) {
            std::ofstream l_osOutputFile;
            l_osOutputFile.open(m_dCutParameters->getOutputFileName().c_str(), std::ios::out);
            for(unsigned int i = 0; i < l_vResults.size(); i++) { l_osOutputFile << l_vResults.at(i) << std::endl; }
            l_osOutputFile.close();
        }
        delete l_dCutter;
    } else {
        this->outputUsage();
        l_bSuccess = false;   
    }
    return l_bSuccess;
}

void Cut::outputUsage() {
    std::cout << std::endl;
    std::cout << "Usage: element cut [-h cutoff] [-c cluster-file]" << std::endl;
    std::cout << "Options/Arguments:" << std::endl;
    // std::cout << "      -k <int>        Number of groups to divide the cluster tree into. Optional." << std::endl;
    std::cout << "      -h <double>     Maximum absolute cutoff for dividing clusters. Optional, defaults to 2." << std::endl;
    std::cout << "      -c <file path>  File path to the cluster XML file (REQD)." << std::endl;
    std::cout << "      -o <file path>  Output file name. Optional, defaults to \"cluster-file.clusters\"." << std::endl;
    std::cout << std::endl;
}
