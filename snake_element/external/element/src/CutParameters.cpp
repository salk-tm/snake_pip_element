#include "Parameters.h"

CutParameters::CutParameters() {
    m_sInputCountFileName   = new std::string("");
    m_sOutputFileName       = new std::string("");
    m_nCutoff               = new double(-1.);
    m_iClusterCount         = new int(-1);
}

CutParameters::~CutParameters() {
    delete m_sOutputFileName;
    delete m_sInputCountFileName;
}

bool CutParameters::verifyParameters() {
    bool l_bParametersGood = true;
    if (*m_sInputCountFileName == "") { l_bParametersGood = false; }
    else {
        if (*m_sOutputFileName == "") { *m_sOutputFileName = *m_sInputCountFileName + ".clusters"; }
        if (*m_nCutoff == -1.0 && *m_iClusterCount == -1) { *m_nCutoff = 2.0; }
    }
    return l_bParametersGood;
}