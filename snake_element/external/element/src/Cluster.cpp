#include "Cluster.h"

Cluster::Cluster(int argc, char** argv) {
    m_dClusterParameters = new ClusterParameters();
    if(argc < 2) { Cluster::outputUsage(); return; }
    else {
        char* l_cParameter;
        for(int i = 1; i < argc; i++) {
            l_cParameter = argv[i];
            if(l_cParameter[0] == '-') {
                switch(l_cParameter[1]) {
                    case 'o': m_dClusterParameters->setOutputFileName(argv[++i]); break;
                    case 'i': m_dClusterParameters->setInputCountFileName(argv[++i]); break;
                    case 'c': m_dClusterParameters->setClusterMethod((ClusterParameters::ClusterMethod)(atoi(argv[++i]))); break;
                    case 'm': m_dClusterParameters->setMetric((ClusterParameters::Metric)(atoi(argv[++i]))); break;
                    default: std::cout << "Error in " << argv[i] << " " << argv[++i] << std::endl;
                }
            }
        }
    }    
    this->run();
}

Cluster::~Cluster() {
    delete m_dClusterParameters;
}

bool Cluster::run() {
    bool l_bSuccess = true;
    if (m_dClusterParameters->verifyParameters()) {
        Elementer* l_dElementer = new Elementer(m_dClusterParameters->getInputCountFileName());
        Clusterer* l_dClusterer = new Clusterer(l_dElementer);
        ClusterTree* l_dClusteredResults = l_dClusterer->getClusteredResults(*(m_dClusterParameters->getMetric()),
                *(m_dClusterParameters->getClusterMethod()));
        l_dClusteredResults->toXML(m_dClusterParameters->getOutputFileName());

        delete l_dElementer;
        delete l_dClusterer;
    } else {
        this->outputUsage();
        l_bSuccess = false;   
    }
    return l_bSuccess;
}

void Cluster::outputUsage() {
    std::cout << std::endl;
    std::cout << "Usage: element cluster [-c 0,1,2,3 ] [-m 0,1,2] [-o output-file] [-i count-file]" << std::endl;
    std::cout << "Options/Arguments:" << std::endl;
    std::cout << "      -m <0,1,2>    Specify which distance metric to use. Optional, defaults to 2:" << std::endl;
    std::cout << "                        0: Levenshtien" << std::endl;
    std::cout << "                        1: Smith-Waterman" << std::endl;
    std::cout << "                        2: Modified Smith-Waterman" << std::endl;
    std::cout << "      -c <0,1,2,...>    Specify which clustering algorithm to use. Optional, defaults to 1:" << std::endl;
    std::cout << "                        0: Single-linkage" << std::endl;
    std::cout << "                        1: Complete-linkage" << std::endl;
    std::cout << "                        2: Weighted-linkage" << std::endl;
    std::cout << "                        3: Median-linkage" << std::endl;
    std::cout << "      -i <file path>  File path to a filtered count file (REQD)." << std::endl;
    std::cout << "      -o <file path>  Output file name. Optional, defaults to \"count-file.xml\"." << std::endl;
    std::cout << std::endl;
}