#ifndef CLUSTER_H
#define	CLUSTER_H

#include <stdlib.h>
#include "Parameters.h"
#include "Clusterer.h"
#include "pugixml.hpp"

class Cluster {
public:
    Cluster(int, char**);
    virtual ~Cluster();
private:
    void outputUsage();
    bool run();
    ClusterParameters*   m_dClusterParameters;
};

#endif	/* CLUSTER_H */
