#include "Parameters.h"

ClusterParameters::ClusterParameters() {
    m_sInputCountFileName   = new std::string("");
    m_iMetric               = new Metric(MODIFIEDSMITHWATERMAN);
    m_iClusterMethod        = new ClusterMethod(COMPLETELINKAGE);
    m_sOutputFileName       = new std::string("");
}

ClusterParameters::~ClusterParameters() {
    delete m_sOutputFileName;
    delete m_sInputCountFileName;
    delete m_iClusterMethod;
    delete m_iMetric;
}

bool ClusterParameters::verifyParameters() {
    bool l_bParametersGood = true;
    if(*m_sInputCountFileName == "") { l_bParametersGood = false; }
    else {
        if(*m_sOutputFileName == "") { *m_sOutputFileName = *m_sInputCountFileName + ".xml"; }
    }
    return l_bParametersGood;
}