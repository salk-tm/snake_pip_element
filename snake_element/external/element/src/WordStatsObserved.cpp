#include "WordStats.h"

WordStatsObserved::WordStatsObserved() {
    m_sWord             = new std::string;
    m_iCount            = new int();
    m_dMean             = new double();
    m_dStddev           = new double();
    m_dProb             = new double();
    m_iHitSeqs          = new int();
    m_nHitPValue        = new double();
    m_nMeanPValue       = new double();
}

WordStatsObserved::WordStatsObserved(std::string p_sWord, int p_iCount, double p_dMean, double p_dStddev, 
        double p_dProb, int p_iHitSeqs, double p_nHitPValue, double p_nMeanPValue) {
    m_sWord             = new std::string(p_sWord);
    m_iCount            = new int(p_iCount);
    m_dMean             = new double(p_dMean);
    m_dStddev           = new double(p_dStddev);
    m_dProb             = new double(p_dProb);
    m_iHitSeqs          = new int(p_iHitSeqs);
    m_nHitPValue        = new double(p_nHitPValue);
    m_nMeanPValue       = new double(p_nMeanPValue);
}

WordStatsObserved::WordStatsObserved(WordStats* p_dWordStats, double p_nHitPValue, double p_nMeanPValue) {
    m_sWord             = new std::string(p_dWordStats->getWord());
    m_iCount            = new int(p_dWordStats->getCount());
    m_dMean             = new double(p_dWordStats->getMean());
    m_dStddev           = new double(p_dWordStats->getStddev());
    m_dProb             = new double(p_dWordStats->getProb());
    m_iHitSeqs          = new int(p_dWordStats->getHitSeqs());
    m_nHitPValue        = new double(p_nHitPValue);
    m_nMeanPValue       = new double(p_nMeanPValue);
}

WordStatsObserved::~WordStatsObserved() {
    delete m_sWord;
    delete m_iCount;
    delete m_dMean;
    delete m_dStddev;
    delete m_dProb;
    delete m_iHitSeqs;
    delete m_nHitPValue;
    delete m_nMeanPValue;
}

bool WordStatsObserved::sortByHitPValue(WordStatsObserved* i, WordStatsObserved* j) { 
    return (i->getHitPValue() < j->getHitPValue());
}

bool WordStatsObserved::sortByMeanPValue(WordStatsObserved* i, WordStatsObserved* j) { 
    return (i->getMeanPValue() < j->getMeanPValue());
}

std::string WordStatsObserved::toString() {
    std::stringstream l_dStringStream;
    l_dStringStream.precision(6);
    l_dStringStream << this->getWord() << "\t";
    l_dStringStream << this->getCount() << "\t";
    l_dStringStream << this->getMean() << "\t";
    l_dStringStream << this->getStddev() << "\t";
    l_dStringStream << this->getProb() << "\t";
    l_dStringStream << this->getHitSeqs() << "\t";
    l_dStringStream << this->getHitPValue() << "\t";
    l_dStringStream << this->getMeanPValue();
    std::string l_sAsString(l_dStringStream.str());
    return l_sAsString;
}
