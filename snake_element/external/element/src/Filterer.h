#ifndef FILTERER_H
#define	FILTERER_H

#include <algorithm>
#include <sstream>
#include <vector>
#include <algorithm>
#include "Parameters.h"
#include "Elementer.h"

class Filterer {
public:
    Filterer(Elementer*);
    virtual ~Filterer();
    std::vector<WordStatsObserved* > getFilteredResults(FilterParameters::FilterStatistic, FilterParameters::FilterType, double);
    std::vector<std::string> getFilteredResultsAsStrings(FilterParameters::FilterStatistic, FilterParameters::FilterType, double);
private:
    static double getPValue(FilterParameters::FilterStatistic, WordStatsObserved*);
    bool initByWordSize();
    std::vector<WordStatsObserved* > getAbsFiltered(FilterParameters::FilterStatistic, double);
    std::vector<WordStatsObserved* > getBenjaminiHochbergFiltered(FilterParameters::FilterStatistic, double);
    std::vector<WordStatsObserved* > getBonferroniHolmFiltered(FilterParameters::FilterStatistic, double);
    std::vector<WordStatsObserved* > getBonferroniFiltered(FilterParameters::FilterStatistic, double);
    std::vector<WordStatsObserved* >*                                           m_dUnfilteredResults;
    boost::unordered_map<unsigned int, std::vector<WordStatsObserved* >* >*     m_dSortedUnfilteredStatsByLength;    
};

#endif	/* FILTERER_H */
