#include "Filterer.h"

Filterer::Filterer(Elementer* p_dElementer) {
    m_dUnfilteredResults                = p_dElementer->getWordStatsResults();
    m_dSortedUnfilteredStatsByLength    = new boost::unordered_map<unsigned int, std::vector<WordStatsObserved* >* >();
    this->initByWordSize();
}

Filterer::~Filterer() {
    delete m_dSortedUnfilteredStatsByLength;
}

bool Filterer::initByWordSize() {
    bool l_bSuccess = true;
    for(unsigned int i = 0; i < m_dUnfilteredResults->size(); i++) {
        unsigned int l_iWordLength = m_dUnfilteredResults->at(i)->getWord().size();
        if(m_dSortedUnfilteredStatsByLength->find(l_iWordLength) == m_dSortedUnfilteredStatsByLength->end()) {
            std::vector<WordStatsObserved* >* l_dTempVector = new std::vector<WordStatsObserved* >();
            l_dTempVector->push_back(m_dUnfilteredResults->at(i));
            m_dSortedUnfilteredStatsByLength->insert(std::make_pair(l_iWordLength, l_dTempVector));
        } else {
            m_dSortedUnfilteredStatsByLength->at(l_iWordLength)->push_back(m_dUnfilteredResults->at(i));
        }
    }    
    
    return l_bSuccess;
}

std::vector<WordStatsObserved* > Filterer::getFilteredResults(
        FilterParameters::FilterStatistic p_dFilterStatistic, FilterParameters::FilterType p_dFilterType, double p_dCutoff) {
    std::vector<WordStatsObserved* > l_vFilteredResults;
    
    // Sort the WordStats
    typedef boost::unordered_map<unsigned int, std::vector<WordStatsObserved* >* > map;
    BOOST_FOREACH(map::value_type i, *m_dSortedUnfilteredStatsByLength) {
        switch(p_dFilterStatistic) {
            case FilterParameters::HIT:
                std::sort(i.second->begin(), i.second->end(), WordStatsObserved::sortByHitPValue);
                break;
            case FilterParameters::MEAN:
                std::sort(i.second->begin(), i.second->end(), WordStatsObserved::sortByMeanPValue);
                break;
            default:
                break;
        }
    }
    
    // Perform the filtering
    switch(p_dFilterType) {
        case FilterParameters::ABS:
    	    l_vFilteredResults = this->getAbsFiltered(p_dFilterStatistic, p_dCutoff);
            break;
        case FilterParameters::BENJAMINIHOCHBERG:
            l_vFilteredResults = this->getBenjaminiHochbergFiltered(p_dFilterStatistic, p_dCutoff);
            break;
        case FilterParameters::BONFERRONIHOLM:
            l_vFilteredResults = this->getBonferroniHolmFiltered(p_dFilterStatistic, p_dCutoff);
            break;
        case FilterParameters::BONFERRONI:
    	    l_vFilteredResults = this->getBonferroniFiltered(p_dFilterStatistic, p_dCutoff);
            break;
        default:
            break;
    }
    
    // Sort the filtered results
    switch(p_dFilterStatistic) {
        case FilterParameters::HIT:
            std::sort(l_vFilteredResults.begin(), l_vFilteredResults.end(), WordStatsObserved::sortByHitPValue);
            break;
        case FilterParameters::MEAN:
            std::sort(l_vFilteredResults.begin(), l_vFilteredResults.end(), WordStatsObserved::sortByMeanPValue);
            break;
        default:
            break;
    }
    
    return l_vFilteredResults;
}

std::vector<std::string> Filterer::getFilteredResultsAsStrings(
    FilterParameters::FilterStatistic p_dFilterStatistic, FilterParameters::FilterType p_dFilterType, double p_dCutoff) {
    std::vector<std::string> l_vStringsResults;
    std::vector<WordStatsObserved*> l_vFilteredResults = this->getFilteredResults(p_dFilterStatistic, p_dFilterType, p_dCutoff);
    for(unsigned int i = 0; i < l_vFilteredResults.size(); i++) {
        WordStatsObserved* l_dTempWordStatsObserved = l_vFilteredResults.at(i);
        l_vStringsResults.push_back(l_dTempWordStatsObserved->toString());
    }    
    return l_vStringsResults;
}

std::vector<WordStatsObserved* > Filterer::getAbsFiltered(FilterParameters::FilterStatistic p_dFilterStatistic, double p_dCutoff) {
    std::vector<WordStatsObserved* > l_vAbsFilteredResults;
    typedef boost::unordered_map<unsigned int, std::vector<WordStatsObserved* >* > map;
    BOOST_FOREACH(map::value_type i, *m_dSortedUnfilteredStatsByLength) {
		for(unsigned int j = 0; j < i.second->size(); j++) {
			WordStatsObserved* l_dCurrentWordStats = i.second->at(j);
			if(Filterer::getPValue(p_dFilterStatistic, l_dCurrentWordStats) <= p_dCutoff) {
				l_vAbsFilteredResults.push_back(l_dCurrentWordStats);
			}
		}
	}
    
    return l_vAbsFilteredResults;
}

std::vector<WordStatsObserved* > Filterer::getBenjaminiHochbergFiltered(FilterParameters::FilterStatistic p_dFilterStatistic,
        double p_dCutoff) {
    std::vector<WordStatsObserved* > l_dBenHochFilteredResults;
    typedef boost::unordered_map<unsigned int, std::vector<WordStatsObserved* >* > map;
    BOOST_FOREACH(map::value_type i, *m_dSortedUnfilteredStatsByLength) {
        bool l_bDone = false;
        long unsigned int l_iDiff = 0, l_iIndex = 0, l_iNumElements = i.second->size();
        while(!l_bDone && l_iDiff < l_iNumElements) {
            l_iIndex = l_iNumElements - 1 - l_iDiff;
            double l_dCurrentPValue = Filterer::getPValue(p_dFilterStatistic, i.second->at(l_iIndex));
            double l_iComparator = (double)(l_iNumElements - l_iDiff) * p_dCutoff / l_iNumElements;
            if(l_dCurrentPValue <= l_iComparator) { l_bDone = true; }
            l_iDiff++;
        }
        if(l_bDone) {
            std::vector<WordStatsObserved* >::iterator l_itPos = i.second->begin() + l_iIndex + 1;
            l_dBenHochFilteredResults.insert(l_dBenHochFilteredResults.end(), i.second->begin(), l_itPos);
        }        
    }
        
    return l_dBenHochFilteredResults;
}

std::vector<WordStatsObserved* > Filterer::getBonferroniHolmFiltered(FilterParameters::FilterStatistic p_dFilterStatistic, 
        double p_dCutoff) {
    std::vector<WordStatsObserved* > l_dBonHolmFilteredResults;
    typedef boost::unordered_map<unsigned int, std::vector<WordStatsObserved* >* > map;
    BOOST_FOREACH(map::value_type i, *m_dSortedUnfilteredStatsByLength) {
        bool l_bDone = false;
        long unsigned int l_iIndex = 0, l_iNumElements = i.second->size();
        while(!l_bDone && l_iIndex < l_iNumElements) {
            double l_dCurrentPValue = Filterer::getPValue(p_dFilterStatistic, i.second->at(l_iIndex));
            double l_iComparator = p_dCutoff / (double)(l_iNumElements - l_iIndex);
            if(l_dCurrentPValue > l_iComparator) { l_bDone = true; }
            l_iIndex++;
        }
        if(l_bDone) {
            std::vector<WordStatsObserved* >::iterator l_itPos = i.second->begin() + l_iIndex - 1;
            l_dBonHolmFilteredResults.insert(l_dBonHolmFilteredResults.end(), i.second->begin(), l_itPos);
        }        
    }
    
    return l_dBonHolmFilteredResults;
}

std::vector<WordStatsObserved* > Filterer::getBonferroniFiltered(FilterParameters::FilterStatistic p_dFilterStatistic,
        double p_dCutoff) {
    std::vector<WordStatsObserved* > l_vBonferroniFilteredResults;
    typedef boost::unordered_map<unsigned int, std::vector<WordStatsObserved* >* > map;
    BOOST_FOREACH(map::value_type i, *m_dSortedUnfilteredStatsByLength) {
		double l_iComparator = p_dCutoff / i.second->size(); 
		for(unsigned int j = 0; j < i.second->size(); j++) {
			WordStatsObserved* l_dCurrentWordStats = i.second->at(j);
			if(Filterer::getPValue(p_dFilterStatistic, l_dCurrentWordStats) <= l_iComparator) {
				l_vBonferroniFilteredResults.push_back(l_dCurrentWordStats);
			}
		}
	}
    
    return l_vBonferroniFilteredResults;
}

double Filterer::getPValue(FilterParameters::FilterStatistic p_dFilterStatistic, WordStatsObserved* p_dWordStats) {
    double l_nPValue;
    switch(p_dFilterStatistic) {
        case FilterParameters::HIT:
            l_nPValue = p_dWordStats->getHitPValue();
            break;
        case FilterParameters::MEAN:
            l_nPValue = p_dWordStats->getMeanPValue();
            break;
        default:
            break;
    }
    
    return l_nPValue;
}
