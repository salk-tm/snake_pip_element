# Element Analysis Workflow

This snakemake workflow was writtent to automate analysis of promoter region sequences. The workflow impliments three related analyses and includes some supplimentary scripts for generating promoter sequences and further analysis. The three main workflow analyses are...

1. Use Element to identify overrpersented kmers among some set of promoters, cluster them into putative motifs
2. Compare putative motifs discovered by element with a reference set of motifs (for transcription factors for example) using tomtom from meme suite
3. Scan promoters for motif matches given some reference motif set using FIMO from meme suite and identify motifs that are overrepresented among some set of promoters

### Documentations
For full documentation on the scripts and workflow, go to the [package readme](snake_element/README.md)

### installation

I recomend using Conda/Mamba for installation and Mamba is essentially required for running the workflow. To install snake_element into a new environment, simply use...

```
wget https://gitlab.com/salk-tm/snake_pip_element/-/raw/master/snake_element/envs/general.yml
conda env create -n snake_element -f general.yml
```

...To install into an existing environment...

```
conda install -c conda-forge -c bioconda python=3 snakemake scikit-learn statsmodels pandas joblib plotly logomaker mamba pip bcbio-gff biopython
pip install git+https://gitlab.com/salk-tm/snake_pip_element.git
```
